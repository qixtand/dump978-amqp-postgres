#!/bin/bash

###
# Setup a RabbitMQ server
###
sudo sh -c 'echo "deb http://www.rabbitmq.com/debian/ testing main" > /etc/apt/sources.list.d/rabbitmq.list'
curl -fsSL https://github.com/rabbitmq/signing-keys/releases/download/2.0/rabbitmq-release-signing-key.asc | sudo apt-key add -
sudo apt-get update -qq

sudo apt-get install -y rabbitmq-server

sudo sh -c 'echo "ulimit -n 1024" >> /etc/default/rabbitmq-server'
sudo service rabbitmq-server restart

# Make the management console available on port 15672
sudo rabbitmq-plugins enable rabbitmq_management

# Install rabbitmqadmin
sudo wget http://localhost:15672/cli/rabbitmqadmin -O /usr/local/bin/rabbitmqadmin
sudo chmod 755 /usr/local/bin/rabbitmqadmin
sudo sh -c 'rabbitmqadmin --bash-completion > /etc/bash_completion.d/rabbitmqadmin'

# Add a user for remote access and promote to admin
sudo rabbitmqctl delete_user guest
sudo rabbitmqctl add_user admin 4G3Gcc9nfjWl4O5VynTeT4Het5rxkUYJ
sudo rabbitmqctl set_user_tags admin administrator

sudo rabbitmqctl add_user dump978 HkNP5p8HQtdBk5nqxWnLBwCIgkIcfhCd
sudo rabbitmqctl set_user_tags dump978 none

sudo rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"
sudo rabbitmqctl set_permissions -p / dump978 ".*" ".*" ".*"

sudo rabbitmqctl set_vm_memory_high_watermark absolute 128MiB


# Prepopulate the rabbitmq.config; it doesn't exist by default
sudo sh -c 'echo "[]." > /etc/rabbitmq/rabbitmq.config'

# Declare a queue
sudo rabbitmqadmin declare queue name=dump978 durable=true -u admin -p 4G3Gcc9nfjWl4O5VynTeT4Het5rxkUYJ


###
# Setup for the push and poll programs
###
#sudo apt-get install python-dev python-pip python-psycopg2 -y
#sudo pip install librabbitmq
